package com.chatlangs.other;

public class Languages {
    private Boolean portuguese;
    private Boolean english;
    private Boolean spanish;

    public Boolean getPortuguese() {
        return portuguese;
    }

    public void setPortuguese(Boolean portuguese) {
        this.portuguese = portuguese;
    }

    public Boolean getEnglish() {
        return english;
    }

    public void setEnglish(Boolean english) {
        this.english = english;
    }

    public Boolean getSpanish() {
        return spanish;
    }

    public void setSpanish(Boolean spanish) {
        this.spanish = spanish;
    }
}
