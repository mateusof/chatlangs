package com.chatlangs.other;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatlangs.R;
import com.chatlangs.other.UserDetails;
import com.chatlangs.other.User;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.Query;

public class FirebaseUserAdapter extends FirebaseListAdapter<User>{

    public FirebaseUserAdapter(Activity activity, Query ref) {
        super(activity, User.class, R.layout.item_list, ref);
    }

    @Override
    protected void populateView(View view, User user, int position) {
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView languages = (TextView) view.findViewById(R.id.languages);
        TextView distance = (TextView) view.findViewById(R.id.distance);
        ImageView avatar = (ImageView) view.findViewById(R.id.avatarImg);

        if (user.getName() != null) {
            name.setText(user.getName());
        }
        if (user.getLanguages() != null) {
            String languagesText = "";
            if(user.getLanguages().getEnglish())
                languagesText += "Inglês";
            if(user.getLanguages().getSpanish())
                languagesText += "\nEspanhol";
            if(user.getLanguages().getPortuguese())
                languagesText += "\nPortuguês";
            languages.setText(languagesText);

            if(user.getAvatar() != null){
                avatar.setImageBitmap(Decode(user.getAvatar()));
            }

            if(user.getLocation() != null){
                distance.setText(distance(UserDetails.latitude, user.getLocation().getLatitude(), UserDetails.longitude, user.getLocation().getLongitude()) + " km");
            }
        }
    }

    private static String distance(double lat1, double lat2, double lon1,
                                  double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c;

        distance = Math.pow(distance, 2);

        return String.format( "%.2f", Math.sqrt(distance) );
    }

    private Bitmap Decode(String strBase64){
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
