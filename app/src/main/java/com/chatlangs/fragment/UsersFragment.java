package com.chatlangs.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.chatlangs.R;
import com.chatlangs.activity.ChatActivity;
import com.chatlangs.other.FirebaseUserAdapter;
import com.chatlangs.other.UserDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class UsersFragment extends Fragment {

    ListView usersList;
    FirebaseUserAdapter userAdapter;

    private FirebaseAuth auth;
    private DatabaseReference userReferenceDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userReferenceDatabase = database.getReference("users");
        auth = FirebaseAuth.getInstance();

        userAdapter = new FirebaseUserAdapter(getActivity(), userReferenceDatabase);


        askForGpsPermissions();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_users, container, false);

        usersList = (ListView) view.findViewById(R.id.usersList);
        usersList.setAdapter(userAdapter);

        usersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserDetails.chatWithId = userAdapter.getRef(position).getKey();
                UserDetails.chatWithName = ""; //TODO
                startActivity(new Intent(getActivity(), ChatActivity.class));
            }
        });
        return view;
    }

    private void askForGpsPermissions(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else {
            configureService();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    configureService();
                } else {
                    Toast.makeText(getActivity(), "Permissão é obrigatória", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public void configureService(){
        try {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    refresh(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) { }

                public void onProviderEnabled(String provider) { }

                public void onProviderDisabled(String provider) { }
            };
            refresh(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }catch(SecurityException ex){
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void refresh(Location location)
    {
        if (location != null) {
            Double latPoint = location.getLatitude();
            Double lngPoint = location.getLongitude();

            if (latPoint != null && lngPoint != null) {
                userReferenceDatabase.child(auth.getCurrentUser().getUid()).child("location").child("latitude").setValue(latPoint);
                userReferenceDatabase.child(auth.getCurrentUser().getUid()).child("location").child("longitude").setValue(lngPoint);
                userReferenceDatabase.child(auth.getCurrentUser().getUid()).child("location").child("lastUpdated").setValue(getCurrentDate());

                UserDetails.latitude = latPoint;
                UserDetails.longitude = lngPoint;
            }
        }
    }

    private String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
