package com.chatlangs.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chatlangs.R;
import com.chatlangs.activity.ChatActivity;
import com.chatlangs.activity.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangeEmailFragment extends Fragment {
    private Button changeEmail;
    private EditText newEmail, password;
    FirebaseUser loggedUser;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loggedUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_change_email, container, false);

        changeEmail = (Button) view.findViewById(R.id.changeEmail);
        newEmail = (EditText) view.findViewById(R.id.new_email);
//        password = (EditText) view.findViewById(R.id.password);

        changeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loggedUser != null && !newEmail.getText().toString().trim().equals("")) {

                    //loggedUser.reauthenticate(EmailAuthProvider.getCredential(loggedUser.getEmail(), password.getText().toString()));

                    loggedUser.updateEmail(newEmail.getText().toString().trim())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getActivity(), "E-mail atualizado", Toast.LENGTH_LONG).show();
                                        ((MainActivity) getActivity()).signOut();
                                    } else {
                                        Toast.makeText(getActivity(), "Falha ao atualizar o E-mail", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                } else if (newEmail.getText().toString().trim().equals("")) {
                    newEmail.setError("E-mail obrigatório");
                }
            }
        });
        return view;
    }
}
