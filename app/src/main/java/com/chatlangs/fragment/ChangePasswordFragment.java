package com.chatlangs.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chatlangs.R;
import com.chatlangs.activity.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordFragment extends Fragment {
    private Button changePassword;
    FirebaseUser loggedUser;
    TextView newPassword;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loggedUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_change_password, container, false);

        changePassword = (Button) view.findViewById(R.id.change_password_button);
//        currentPassword = (EditText) view.findViewById(R.id.currentPassword);
        newPassword = (EditText) view.findViewById(R.id.newPassword);

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loggedUser != null && !newPassword.getText().toString().trim().equals("")) {
                    if (newPassword.getText().toString().trim().length() < 6) {
                        newPassword.setError("Senha deve conter no m;inimo 6 caracteres");
                    } else {
                        //loggedUser.reauthenticate(EmailAuthProvider.getCredential(loggedUser.getEmail(), currentPassword.getText().toString()));

                        loggedUser.updatePassword(newPassword.getText().toString().trim())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(getActivity(), "Senha atualizada", Toast.LENGTH_SHORT).show();
                                            ((MainActivity) getActivity()).signOut();
                                        } else {
                                            Toast.makeText(getActivity(), "Falha ao atualizar a senha", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                } else if (newPassword.getText().toString().trim().equals("")) {
                    newPassword.setError("Senha obrigatória");
                }
            }
        });

        return view;
    }
}
